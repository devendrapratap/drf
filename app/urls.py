from app import views_function as fviews
from app import views_class as cviews
from app import views_auth as aviews

from django.conf.urls import url

from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = format_suffix_patterns([
    url(r'^1/snippets/$', fviews.snippet_list_se),
    url(r'^1/snippets/(?P<pk>[0-9]+)/$', fviews.snippet_detail_se),
    url(r'^2/snippets/$', fviews.snippet_list_rr),
    url(r'^2/snippets/(?P<pk>[0-9]+)/$', fviews.snippet_detail_rr),
    url(r'^3/snippets/$', cviews.SnippetList.as_view()),
    url(r'^3/snippets/(?P<pk>[0-9]+)/$', cviews.SnippetDetail.as_view()),
    url(r'^4/snippets/$', cviews.SnippetListMixins.as_view()),
    url(r'^4/snippets/(?P<pk>[0-9]+)/$', cviews.SnippetDetailMixins.as_view()),
    url(r'^5/snippets/$', cviews.SnippetListGeneric.as_view()),
    url(r'^5/snippets/(?P<pk>[0-9]+)/$',
        cviews.SnippetDetailGeneric.as_view()),

    url(r'^login/$', aviews.IamLoginView.as_view())
])
