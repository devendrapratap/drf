
from __future__ import unicode_literals

from app import forms
from app.util import JSONResponseMixin

from django.contrib import messages
from django.contrib.auth import get_user_model, login as auth_login, logout as auth_logout, update_session_auth_hash
from django.contrib.auth import views, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin

from django.core.urlresolvers import reverse_lazy

from django.shortcuts import render, resolve_url

from django.utils.decorators import method_decorator
from django.utils.translation import gettext as _

from django.views import generic
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt, csrf_protect, ensure_csrf_cookie
from django.views.decorators.debug import sensitive_post_parameters

from django.conf import settings
User = get_user_model()


@method_decorator(ensure_csrf_cookie, name='get')
@method_decorator(ensure_csrf_cookie, name='post')
class IamLoginView(views.LoginView, JSONResponseMixin):
    form_class = forms.IamAuthenticationForm

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)

    def get(self, request, *args, **kwargs):
        return self.render_to_response({'form': self.get_form().as_dict()})

    def form_valid(self, form):
        return self.render_to_response({'status': 'login ok'})

    def form_invalid(self, form):
        return self.render_to_response({'status': 'invalid login'})


class IamLogoutView(views.LogoutView, JSONResponseMixin):
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        auth_logout(request)
        return self.render_to_json_response({'status': 'logout ok'}, **kwargs)
