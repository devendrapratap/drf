import logging
from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth.forms import PasswordResetForm, AuthenticationForm, SetPasswordForm, AdminPasswordChangeForm
from django.core.exceptions import ValidationError
from django.utils.translation import gettext, gettext_lazy as _
from django.forms import forms
from django.contrib.auth import get_user_model

User = get_user_model()

class FormJSON(object):
    """docstring for FormJSON."""
    def __init__(self, arg):
        super(FormJSON, self).__init__()

    def field_type(self, field):
        """
           Converts django Model field to html input type.
            :return string
           Usage::
               {% load html_input_type %}
               {% field | field_type %}
           Examples::
               {% for field in form %}
                    <input id="{{ field.id_for_label }}" maxlength="155"
                           name="{{ field.html_name }}" placeholder="{{ field.label }}"
                           type="{{field | field_type}}"/>

               {% endfor %}
        """
        input_type = {
            "TextInput": "text",
            "NumberInput": "number",
            "EmailInput": "email",
            "URLInput": "url",
            "PasswordInput": "password",
            "HiddenInput": "hidden",
            "MultipleHiddenInput": "hidden",
            "FileInput": "file",
            "ClearableFileInput": "file",
            None: "text",
        }
        try:
            field_name = field.field.widget.__class__.__name__
            return input_type[field_name]
        except Exception as ex:
            logging.error("{}".format(str(ex)))
            return input_type[None]

    def get_form_field(self):
        forms = self
        form_json = {
            'name': self.__class__.__name__,
            'fields': []
        }
        forms_fields = []
        try:
            for field in forms:
                fields = {
                    "name": field.html_name,
                    "id": field.id_for_label,
                    "label": field.label,
                    "type": self.field_type(field)
                }
                forms_fields.append(fields)
        except KeyError as exception:
            logging.error("{}", str(exception))

        form_json['fields'] = forms_fields
        return form_json

    def as_dict(self):
        return self.get_form_field()

    def __dir__(self):
        return self.get_form_field()

class IamAuthenticationForm(AuthenticationForm, FormJSON):
    # The following function needs to be overwritten because
    # the default flow in the library does not get to this pont.
    def confirm_login_allowed(self, user):
        """
        Check for a valid email and phone
        """
        print("Confirm login")
        if not user.is_active or not user.email or not user.phone_number:
            print("User to be logged out") # uncomment the following
            # raise forms.ValidationError(
            #     self.error_messages['inactive'],
            #     code='inactive',
            #     )
